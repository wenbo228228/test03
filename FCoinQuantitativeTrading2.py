# -*- coding:utf-8 -*-
# self buy and sell ft/usdt
import time
import requests
from FCoinAPI import api_controller
import datetime
import math
from config import Api

print('\n')
print('----Program started...')
print('\n\n')

api_key = Api['key']
api_secret = Api['secret']
api = api_controller(api_key, api_secret)


def get_current_time():
    return str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))


def get_ticker(symbol):
    r = requests.get('https://api.fcoin.com/v2/market/ticker/{}'.format(symbol))
    # print(r.json())
    max_buy = r.json()['data']['ticker'][2]
    min_sell = r.json()['data']['ticker'][4]
    # print(max_buy,min_sell)
    return max_buy, min_sell


def get_balance_ft_usdt():
    balance = api.get_balance()
    for i in balance['data']:
        if i['currency'] == 'ft':
            balance_ft = float(i['available'])
        if i['currency'] == 'usdt':
            balance_usdt = float(i['available'])
    return balance_ft, balance_usdt


def cancel_unfilled_order():
    order = api.get_orders_list('ftusdt', 'submitted,partial_filled', '', '2', 20)
    if len(order['data']) > 0:
        for i in order['data']:
            api.cancle_order(i['id'])
        return True
    return False


def re_balance_limit_price(balance_ft, balance_usdt, min_sell):
    global transaction_total_amount
    printed_notice = False
    while 1:
        balance_usdt_by_ft = balance_usdt / min_sell
        ft_usdt_gap = abs(balance_ft - balance_usdt_by_ft) / ((balance_ft + balance_usdt_by_ft) / 2)
        if ft_usdt_gap > 0.2:
            if printed_notice == False:
                print('\n\n')
                print('--------------{}--------------'.format(get_current_time()))
                print('#limit rebalancing...')
                printed_notice = True
            max_buy, min_sell = get_ticker('ftusdt')
            middle_price = (min_sell + max_buy) / 2
            middle_price = math.floor(middle_price * 1000000) / 1000000
            balance_usdt_by_ft = balance_usdt / middle_price
            value_ft_before_rebalance = balance_ft + balance_usdt_by_ft
            if balance_ft > balance_usdt_by_ft:
                amount_ft_to_sell = balance_ft - (balance_ft + balance_usdt_by_ft) / 2
                amount_ft_to_sell = math.floor(amount_ft_to_sell * 100) / 100
                api.create_order('ftusdt', 'sell', 'limit', middle_price, amount_ft_to_sell)
                transaction_total_amount = transaction_total_amount + amount_ft_to_sell
            else:
                amount_ft_to_buy = balance_usdt_by_ft - (balance_ft + balance_usdt_by_ft) / 2
                amount_ft_to_buy = math.floor(amount_ft_to_buy * 100) / 100
                api.create_order('ftusdt', 'buy', 'limit', middle_price, amount_ft_to_buy)
            time.sleep(5)
            # deal with submitted or partial_filled order
            cancel_unfilled_order()
            time.sleep(5)
            balance_ft, balance_usdt = get_balance_ft_usdt()
            value_ft_after_rebalance = balance_ft + balance_usdt / middle_price
            value_loss = (value_ft_before_rebalance - value_ft_after_rebalance) / value_ft_before_rebalance
            print("value loss:{:.4%}".format(value_loss))
        else:
            # no need to rebalance
            break
    return balance_ft, balance_usdt


def re_balance_market_price(balance_ft, balance_usdt, min_sell):
    global transaction_total_amount
    printed_notice = False
    while 1:
        balance_usdt_by_ft = balance_usdt / min_sell
        ft_usdt_gap = abs(balance_ft - balance_usdt_by_ft) / ((balance_ft + balance_usdt_by_ft) / 2)
        if ft_usdt_gap > 0.2:
            if printed_notice == False:
                print('\n\n')
                print('--------------{}--------------'.format(get_current_time()))
                print('#market rebalancing...')
                printed_notice = True
            max_buy, min_sell = get_ticker('ftusdt')
            if balance_ft > balance_usdt_by_ft:
                balance_usdt_by_ft = balance_usdt / max_buy
                value_ft_before_rebalance = balance_ft + balance_usdt_by_ft
                amount_ft_to_sell = balance_ft - (balance_ft + balance_usdt_by_ft) / 2 - 10
                amount_ft_to_sell = math.floor(amount_ft_to_sell * 100) / 100
                api.create_order('ftusdt', 'sell', 'market', '', amount_ft_to_sell)
                transaction_total_amount = transaction_total_amount + amount_ft_to_sell
            else:
                balance_usdt_by_ft = balance_usdt / max_buy
                value_ft_before_rebalance = balance_ft + balance_usdt_by_ft
                amount_ft_to_buy = balance_usdt_by_ft - (balance_ft + balance_usdt_by_ft) / 2 - 10
                amount_ft_to_buy = math.floor(amount_ft_to_buy * 100) / 100
                api.create_order('ftusdt', 'buy', 'market', '', amount_ft_to_buy)
            time.sleep(5)
            # deal with submitted or partial_filled order
            cancel_unfilled_order()
            balance_ft, balance_usdt = get_balance_ft_usdt()
            value_ft_after_rebalance = balance_ft + balance_usdt / min_sell
            value_loss = (value_ft_before_rebalance - value_ft_after_rebalance) / value_ft_before_rebalance
            print("value loss:{:.4%}".format(value_loss))
        else:
            # no need to rebalance
            break
    return balance_ft, balance_usdt


def create_order(sample, side, type, price, amount):
    api.create_order(sample, side, type, price, amount)


while 1:
     try:
        max_buy, min_sell = get_ticker('ftusdt')
        balance_ft_before, balance_usdt_before = get_balance_ft_usdt()
        balance_ft_before, balance_usdt_before = re_balance_limit_price(balance_ft_before, balance_usdt_before,
                                                                        min_sell)
        while 1:
            # check snapshot time point
            now_minites = datetime.datetime.now().strftime("%M")
            now_minites = int(now_minites)
            if now_minites >= 55:
                print('\n\n')
                print('--------------{}--------------'.format(get_current_time()))
                print('prepare for snapshot...')
                # buy ft
                try_count = 0
                while 1:
                    max_buy, min_sell = get_ticker('ftusdt')
                    middle_price = (min_sell + max_buy) / 2
                    middle_price = math.floor(middle_price * 1000000) / 1000000
                    transaction_amount = balance_usdt_before / middle_price
                    transaction_amount = math.floor(transaction_amount * 100) / 100
                    api.create_order('ftusdt', 'buy', 'limit', middle_price, transaction_amount)
                    time.sleep(5)
                    # deal with submitted or partial_filled order
                    time.sleep(5)
                    balance_ft_before, balance_usdt_before = get_balance_ft_usdt()
                    if balance_usdt_before < 1:
                        break
                    else:
                        try_count = try_count + 1
                        if try_count > 6:
                            max_buy, min_sell = get_ticker('ftusdt')
                            # 市价买入，压低数量，防止下单数量过大
                            transaction_amount = max(balance_usdt_before / min_sell - 10, 0)
                            transaction_amount = math.floor(transaction_amount * 100) / 100
                            api.create_order('ftusdt', 'buy', 'market', '', transaction_amount)
                            break
                print(get_current_time() + '     waiting for snap shot...')
                now_minites = datetime.datetime.now().strftime("%M")
                now_minites = int(now_minites)
                while now_minites >= 55:
                    time.sleep(60)
                    now_minites = datetime.datetime.now().strftime("%M")
                    now_minites = int(now_minites)
                while now_minites <= 3:
                    print('snap shot finished...')
                    time.sleep(60)
                    now_minites = datetime.datetime.now().strftime("%M")
                    now_minites = int(now_minites)
                # rebanlance
                balance_ft_before, balance_usdt_before = get_balance_ft_usdt()
                balance_ft_before, balance_usdt_before = re_balance_limit_price(balance_ft_before, balance_usdt_before,
                                                                                min_sell)

            max_buy, min_sell = get_ticker('ftusdt')
            value_before = balance_ft_before + balance_usdt_before / min_sell
            value_before = math.floor(value_before * 100) / 100
            print('\n\n')
            print('--------------{}--------------'.format(get_current_time()))
            print('#Before order   balance_ft:{}   balance_usdt:{}     --value:{} ft'.format(balance_ft_before,
                                                                                             balance_usdt_before,
                                                                                             value_before))
            print('#min_sell:{}  max_buy:{}  gap:{}'.format(min_sell, max_buy, min_sell - max_buy))
            # if min_sell - max_buy > 0.0001:
            middle_price = (min_sell + max_buy) / 2
            middle_price = math.floor(middle_price * 1000000) / 1000000
            transaction_amount = min(balance_usdt_before / middle_price, balance_ft_before)
            transaction_amount = math.floor(transaction_amount * 100) / 100
            api.create_order('ftusdt', 'buy', 'limit', middle_price, transaction_amount)
            api.create_order('ftusdt', 'sell', 'limit', middle_price, transaction_amount)
            # else:
            #     transaction_amount = min(balance_usdt_before / min_sell, balance_ft_before)
            #     transaction_amount = math.floor(transaction_amount * 100) / 100
            #     api.create_order('ftusdt', 'buy', 'limit', min_sell, transaction_amount)
            #     api.create_order('ftusdt', 'sell', 'limit', max_buy, transaction_amount)
            time.sleep(5)
            # deal with submitted or partial_filled order
            order_cancelled = cancel_unfilled_order()
            # if order_cancelled==True:

            time.sleep(5)
            balance_ft, balance_usdt = get_balance_ft_usdt()
            value = balance_ft + balance_usdt / min_sell
            value = math.floor(value * 100) / 100
            print('#After order   balance_ft:{}   balance_usdt:{}      --value:{} ft'.format(balance_ft, balance_usdt,
                                                                                             value))
            value_loss = (value_before - value) / value
            value_loss = math.floor(value_loss * 1000000) / 1000000
            if value_loss < 0.0011:
                # normal consume
                print("value loss:{:.4%}".format(value_loss))
                balance_ft, balance_usdt = re_balance_limit_price(balance_ft, balance_usdt, min_sell)
            else:
                print('value loss too much,rechecking...')
                time.sleep(5)
                balance_ft, balance_usdt = get_balance_ft_usdt()
                value = balance_ft + balance_usdt / min_sell
                value = math.floor(value * 100) / 100
                value_loss = (value_before - value) / value
                value_loss = math.floor(value_loss * 1000000) / 1000000
                print('#recheck complete   balance_ft:{}   balance_usdt:{}      --value:{} ft'.format(balance_ft,
                                                                                                      balance_usdt,
                                                                                                      value))
                if value_loss < 0.0011:
                    print('value loss:{:.4%}'.format(value_loss))
                else:
                    print('value loss:{:.4%}    value loss too much,probably server error'.format(value_loss))
                    raise Exception('Too much loss')
            balance_ft_before = balance_ft
            balance_usdt_before = balance_usdt
        # time.sleep(1)
        print('total transaction amount:{} ft'.format(transaction_total_amount))
     except:
       print('\n---------------exception---------------\n')
